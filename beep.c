// 440 Hz = 4545
// bpms = 1 beat / x ms
// bpm = 60 beats / ((x ms) / 1000) seconds = (60 * 1000) beats / x ms
// note length (ms) = 60000 beats / y bpm

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define CONCERT_A 440
#define BPM 180

#include "note_defines.h"

#define OSC_RATE_CONST 1000000

volatile unsigned int song[256] = {Gb4, ST, Ab4, ST, Bb5, ET, Bb5, ST, 0, ST, Db5, ST, 0, ST, Db5, QT, 0, 0};

volatile unsigned char resting = 1;
volatile unsigned int note = 440;

void initialize_timer_1(void) {
  TCCR1B = 2;                                           // Increment timer at 2MHz
  TIMSK1 |= (1 << OCIE1A);                              // Enable output compare match
  sei();                                                // Enable global interrupts
}

ISR(TIMER1_COMPA_vect) {
  if(resting == 0) {
    PORTD ^= (1 << PD3);
  }
  OCR1A += OSC_RATE_CONST / note;
}

void play(unsigned int to_play, unsigned int length) {
  note = to_play;
  _delay_ms(length);
}

void rest(unsigned int length) {
  resting = 1;
  _delay_ms(length);
  resting = 0;
}

void start() {
  resting = 0;
}

void cut() {
  resting = 1;
}

int main(void) {
  DDRD |= (1 << PD3);
  initialize_timer_1();
  start();
  while(1) {
    unsigned int current = 0;
    for(current = 0; !((song[current] == 0) && (song[current + 1] == 0)); current += 2) {
      if(song[current]) {
        play(song[current], song[current + 1]);
      } else {
        rest(song[current + 1]);
      }
    }
  }
  return 0;
}